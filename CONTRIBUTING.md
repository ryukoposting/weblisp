## Contents

 - [Philosophy](#Philosophy)
 - [Contributor Requirements](#Requirements): Rules that contributed code
   MUST follow in order for it to be accepted.
 - [Best Practices](#Practices): Suggestions to help write good,
   spaghetti-free code.
 - [Tests, Linting, and Continuous Integration](#TestsCI): Explanation of the
   Weblisp testing framework, lint script, and GitLab GI implementation.
 - [Code of Conduct](#CoC)
 - [Documentation](#Documentation)
 - [Licensing Considerations](#Licensing)

<a name="Philosophy"></a>
## Design Philosophy

 - The complexity a feature adds to the end product must be in proportion to the
   value that feature provides.
 - The complexity a feature adds to the codebase must be in proportion to the
   value that feature provides.
 - The single most valuable feature in any piece of software is reliability.
 - Reliability and simplicity are fundamentally related. 
 - The master branch is sacred.

<a name="Requirements"></a>
## Contributor Requirements

Items under "Contributor Requirements" are, well... required. Contributors
who wish to have their pull requests accepted MUST follow the standards
listed in this section.

Merge requests that do not conform with the rules under "Contributor Requirements"
will not be accepted. However, if a merge request is rejected, it will not be held
against the contributor in future merge requests.

### Standards Compliance and Defined Behavior

Weblisp complies with the ISO C11 standard. Contributions using language features
that are not part of the C11 standard will not be accepted until they are fully
compliant. Contributions using compiler-specific extensions also will not be
accepted.

Code containing behaviors that are undefined under the ISO C11 standard will also
not be accepted. Contributors should be aware of what constitutes "undefined
behavior."

<a name="style_required"></a>
### Code Style Requirements

Generally speaking, there is no "best code style." The best-written code is
whatever code is the most legible; no single set of standards is necessarily better
than any other at attaining readability. Because of this, there are only a
handful of requirements related to code style. However, while code style
requirements are sparse, they are strongly enforced. Contributors who wish to have
their work accepted MUST conform to the following 3 rules:

 - Indent with 4 spaces.
 - Placement of curly brackets and control block keywords must follow the style
   shown in [section 3.1 of the Linux kernel style guide](https://www.kernel.org/doc/html/v4.12/process/coding-style.html#placing-braces-and-spaces).
 - UNIX-style line endings (line feed, AKA  `\n`) are required.

Suggestions for writing clean, legible code are included under
"[Code Style Best Practices](#style_pref)."

### License Boilerplate

Weblisp is licensed under version 2.0 of the Apache License. Appendix A of this
license shows boilerplate text that should be displayed at the top of
all source code files. Although not strictly required by the license, it is
a requirement of the Weblisp project that this boilerplate text be included in
all files inside the `weblisp/` directory of the project.

### Header file format

Header files always start with the boilerplate text shown in the project license.
Immediately following the license info, all header files should contain the
following:

```C
#ifndef _<name of header file in capital letters>_H_
#define _<name of header file in capital letters>_H_

#ifdef __cplusplus
extern "C" {
#endif
```

All header files should end with:

```C
#ifdef __cplusplus
}
#endif

#endif
```

Declarations made in header files are not indented. Your editor may pick up
on the curly brackets inside the ifdefs, and try to indent things. Do NOT
let your editor do this!

<a name="Practices"></a>
## Best Practices

Contributors are not strictly required to follow the standards listed in
Best Practices. However, adherence to these best practices is strongly preferred.

<a name="style_pref"></a>
### Code Style Best Practices

Follow those general rules, and do your best to make things legible. Where
it isn't clear what a piece of code is doing, throw in a brief comment. Whatever
you do, just be consistent.

 - Leave whitespace to help distinguish logical program flow. For example,
   after a `break` in a switch-case.
 - Case statements inside a switch-case are not indented beyond the switch
   statement.
 - Use `/**/` comments instead of `//`.
 - There is nothing wrong with pointer arithmetic. There is also nothing wrong
   with nesting an assignment inside of an expression. However, there are a lot
   of things wrong with doing a ton of that stuff in a single line.
 - Stick to a maximum line width of 80 characters wherever possible.

The code style rules shown here are just recommendations (though it is
strongly preferred that you follow them). There are a handful of strict style
requirements under "[Code Style Requirements](#style_required)."

### The 7-Point Rule

One of the most common complaints with procedural code is that it often
creates gigantic tangles of nested control blocks. While the structure of
Weblisp is mostly procedural, we hope to avoid this pitfall. If you feel like
a block of code might be getting a little too complex, the 7-Point Rule can
provide a helpful frame of reference. Here's how it works:

 - `if`, `else`, and `else if` cost 2 points.
 - `while`, `do while`, and `switch` cost 3 points.
 - `for` costs 4 points.

Sum the points of nested control blocks together. If the sum is greater than
7 points, then that section of code would likely benefit from being divided
into smaller parts. The code snippet below shows a simple example of point
counts at different levels of nesting:

```C
/* 0 points */

while (x >= 2) {
    /* 3 points */
    
    if ((*c != 0) && (y != 10)) {
        /* 5 points */
        
        if (y == 0) {
            /* 7 points */
            return 0;
        }
    
    } else if (x < 6) {
        /* 5 points */
        
        while (y--) {
            /* 8 points... uh-oh! */
            ++c;
        }
    }
}
```

Understand that this is a general rule, and should not be taken as gospel.
Indeed, there are times when a bit of deeper nesting can actually make code more
readable than if it were divided into smaller parts. This is uncommon, however,
and the 7-Point Rule provides a reasonable reference point for deciding whether
a piece of code should be refactored.

<a name="TestsCI"></a>
## Tests, Linting, and Continuous Integration

### Test Framework

The test framework under the [tests/](./tests) directory of the project contains
test programs. These tests are run by GitLab CI every time a contributor pushes
to a remote branch. All non-trivial features of Weblisp should have a test program.

The test framework consists of [test.h](./tests/test.h) and the makefiles under
[tests/build/](./tests/build). The tests have a separate set of vpaths and source
file lists from the other builds! When you add a new file to the project, make sure
to update the contents of both [build/Makefile.include](./build/Makefile.include)
AND [tests/build/Makefile.include](./tests/build/Makefile.include).

The contents of test.h should be fairly self-explanatory- special assertion
statements are used for tests, and test programs should be added to `TEST_SOURCES`
inside tests/build/Makefile.include.

### Lints

[build/lintscript.sh](build/lintscript.sh) is a simple shell script for linting
Weblisp. It shows an approximate line count for the project (adjusted for license
and header boilerplate), all TODO comments in the project, and runs
[cppcheck](https://manpages.debian.org/testing/cppcheck/cppcheck.1.en.html) on
the project. The lint script can be very helpful in identifying problem points in
the project- it is recommended that you use it regularly.

### GitLab CI

Weblisp leverages Gitlab CI for automated testing. `.gitlab-ci.yml` describes the
CI pipeline (more detail can be found in GitLab's documentation). **Code that does
not pass all tests, or otherwise fails in the CI pipeline, will not be pushed to
master.**

<a name="CoC"></a>
## Code of Conduct

All contributions are welcome, but are merged at the discretion of the core
contributor(s). Contributions to the project will be judged on their merits
without respect to a contributor's nationality, ethnicity, identity, demographic,
or publicly or privately held beliefs, opinions, ideology, or faith. Most
communications within the project should be limited to project planning, development,
bugfixing, or other relevant topics; for off-topic discussions, contributors are
expected to use good judgement and to avoid intentionally abusive behavior. Conflict
should be resolved at the lowest level possible with minimal disruption to the
project. Core contributor(s) reserve the right to request that a contributor alter
their behavior. Nothing in this code of conduct should be construed in such a manner
that it infringes upon any contributor's freedom of expression.

This code of conduct is an adaption of one written by [Richard Davis](https://gitlab.com/d3d1rty)
for [gbud](https://gitlab.com/d3d1rty/gbud).
[Used with permission](https://lewd.pics/p/9t7w.png).

<a name="Documentation"></a>
## Documentation

Documentation for Weblisp can be divided into two general types: documentation
for Weblisp users, and documentation for Weblisp developers. This dichotomy
should always be considered when writing documentation, i.e. the average user
trying to read the Weblisp `man` pages neither needs nor wants to read an
in-depth technical description of how the application works under the hood.

Developer documentation should be put in the
[wiki](https://gitlab.com/ryukoposting/weblisp/wikis/home).
Weblisp wiki pages are written in
[version 0.28 of CommonMark](https://spec.commonmark.org/0.28/). The file you
are reading right now (CONTRIBUTING.md) is compliant with this standard, and
may serve as a helpful reference when writing documentation for Weblisp.

User documentation consists of:

 - the `man` pages, located in [docs/man/](./docs/man)
 - the language reference, located in [docs/lang/](./docs/lang).

`man` pages are written in troff.

<a name="Licensing"></a>
## Licensing Considerations

Weblisp is licensed under version 2.0 of the Apache License. This is an
FSF and OSI-approved license with provisions specifically relating to
indemnification and patents. It is
[compatible with the the GNU GPLv3](https://www.gnu.org/philosophy/license-list.html#apache2).
All contributors should be familiar with the license.
