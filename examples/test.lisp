"
<head>
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<style>

@media screen and (orientation: portrait) {
	#wrapper {
		display: grid;
		grid-template-columns: auto;
		grid-template-rows: 40px 2px auto auto;
	}
	.title {
		grid-column-start: 1;
		grid-column-end: -1;
		grid-row-start: 1;
		grid-row-end: 1;
		background-color: lightblue;
	}
	.sitemap {
		padding: 10px;
		grid-column-start: 1;
		grid-column-end: -1;
		grid-row-start: 3;
		grid-row-end: 4;
        display: grid;
	}
	.main {
		padding: 10px;
		grid-column-start: 1;
		grid-column-end: -1;
		grid-row-start: 4;
		grid-row-end: 5;
	}
	div.maplink {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        background-color: yellow;
        font-size: 20px;
    }
    div.sidebar {
        display: none;
    }
}

@media screen and (orientation: landscape) {
	#wrapper {
        padding-left: 8.09%;
		display: grid;
		grid-template-columns: 8.09% auto 8.09%;
		grid-template-rows: 40px 2px auto;
	}
	.title {
		grid-column-start: 1;
		grid-column-end: -1;
		grid-row-start: 1;
		grid-row-end: 2;
		background-color: lightblue;
	}
	.main {
		padding: 10px;
		grid-column-start: 2;
		grid-column-end: 3;
		grid-row-start: 3;
		grid-row-end: 3;
        background-color: lightgreen;
	}
	div.maplink {
        display: none;
    }
    div.sidebar {
        width: 160px;
        align: left;
        position: -webkit-sticky;
        position: sticky;
        top: 60px;
    }
}

* {
	font-family: Helvetica, sans-serif !important;
    margin: 0px;
}

body {
	background-color: #EEEEEE;
}

div.sitehead {
    padding: 20px;
}

</style>
</head>
<body>

<div class=\"sitehead\">
<h1>Site Head</h1>
</div>

<div class=\"maplink\">
Site Map
</div>

<div class=\"sidebar\">
    <p>asdf</p>
    <p>qwer</p>
</div>

<div id=\"wrapper\">
	<div class=\"title\">
		<h1>hello world!</h1>
		
	</div>

	<div class=\"main\">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Phasellus imperdiet, nulla et dictum interdum, nisi lorem 
		egestas odio, vitae scelerisque enim ligula venenatis dolor.
		Maecenas nisl est, ultrices nec congue eget, auctor vitae massa.
		Fusce luctus vestibulum augue ut aliquet. Mauris ante ligula,
        facilisis sed ornare eu, lobortis in odio. Praesent convallis
        urna a lacus interdum ut hendrerit risus congue. Nunc sagittis
        dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero
        sed nunc venenatis imperdiet sed ornare turpis. Donec vitae
        dui eget tellus gravida </p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Phasellus imperdiet, nulla et dictum interdum, nisi lorem 
		egestas odio, vitae scelerisque enim ligula venenatis dolor.
		Maecenas nisl est, ultrices nec congue eget, auctor vitae massa.
		Fusce luctus vestibulum augue ut aliquet. Mauris ante ligula,
        facilisis sed ornare eu, lobortis in odio. Praesent convallis
        urna a lacus interdum ut hendrerit risus congue. Nunc sagittis
        dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero
        sed nunc venenatis imperdiet sed ornare turpis. Donec vitae
        dui eget tellus gravida </p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Phasellus imperdiet, nulla et dictum interdum, nisi lorem 
		egestas odio, vitae scelerisque enim ligula venenatis dolor.
		Maecenas nisl est, ultrices nec congue eget, auctor vitae massa.
		Fusce luctus vestibulum augue ut aliquet. Mauris ante ligula,
        facilisis sed ornare eu, lobortis in odio. Praesent convallis
        urna a lacus interdum ut hendrerit risus congue. Nunc sagittis
        dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero
        sed nunc venenatis imperdiet sed ornare turpis. Donec vitae
        dui eget tellus gravida </p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Phasellus imperdiet, nulla et dictum interdum, nisi lorem 
		egestas odio, vitae scelerisque enim ligula venenatis dolor.
		Maecenas nisl est, ultrices nec congue eget, auctor vitae massa.
		Fusce luctus vestibulum augue ut aliquet. Mauris ante ligula,
        facilisis sed ornare eu, lobortis in odio. Praesent convallis
        urna a lacus interdum ut hendrerit risus congue. Nunc sagittis
        dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero
        sed nunc venenatis imperdiet sed ornare turpis. Donec vitae
        dui eget tellus gravida </p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		Phasellus imperdiet, nulla et dictum interdum, nisi lorem 
		egestas odio, vitae scelerisque enim ligula venenatis dolor.
		Maecenas nisl est, ultrices nec congue eget, auctor vitae massa.
		Fusce luctus vestibulum augue ut aliquet. Mauris ante ligula,
        facilisis sed ornare eu, lobortis in odio. Praesent convallis
        urna a lacus interdum ut hendrerit risus congue. Nunc sagittis
        dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero
        sed nunc venenatis imperdiet sed ornare turpis. Donec vitae
        dui eget tellus gravida </p>
	</div>
</div>
</body>
"
