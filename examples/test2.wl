;; (style
;;     (let sans-serif
;;         (list "Helvetica" "sans-serif"))
;;     
;;     (let serif
;;         (list "Georgia" "serif"))
;;     
;;     (let sitehead
;;            (list
;;                 (col-start 1)
;;                 (col-end -1)
;;                 (row-start 1)
;;                 (row-end 2)
;;                 (bg "indigo")))
;;     
;;     (portrait
;;         (list auto)
;;         (list auto auto auto auto)
;;         sitehead
;;         (let title
;;             (list
;;                 (col-start 1)
;;                 (col-end -1)
;;                 (row-start 2)
;;                 (row-end 3)))
;;         (let content
;;             (list
;;                 (col-start 1)
;;                 (col-end -1)
;;                 (row-start 3)
;;                 (row-end -1)))
;;         (let maplink
;;             (list
;;                 (sticky (bottom 0))
;;                 (col-start 1)
;;                 (col-end -1)
;;                 (row-start 4)
;;                 (row-end -1)
;;                 (bg (rgba 180 177 185 0.85)))))
;; 
;;     (landscape
;;         (cons (minmax (px 140) 0.0809) auto 0.0809)
;;         (cons (px 48) (px 40) auto)
;;         sitehead
;;         (let title
;;             (list
;;                 (col-start 2)
;;                 (col-end -2)
;;                 (row-start 2)
;;                 (row-end 3)))
;;         (let content
;;             (list
;;                 (col-start 2)
;;                 (col-end -2)
;;                 (row-start 3)
;;                 (row-end -1)))
;;         (let sidebar
;;             (list
;;                 (col-start 1)
;;                 (col-end 2)
;;                 (min-width (minmax (px 140) 0.0809))
;;                 (padding
;;                     (left 10)
;;                     (right 10))))))

(let (div-id id txt)
    (list 
        (raw "<div id=\"") id (raw "\">")
        (list txt)
        (raw "</div>")))

(let (div-id-class id class txt)
    (list 
        (raw "<div id=\"") id (raw "\" class=\"") class (raw "\">")
        (list txt)
        (raw "</div>")))

(let (div-class class txt)
    (list
        (raw "<div class=\"") class (raw "\">")
        (list txt)
        (raw "</div>")))

(body
    (div-class "test" (ul (li "foo")))
    (p "this is a test\"")
    (p "hello world"))


;; (body 
;;    (p "hello, world!" (st "stri\"keth\\rough text") "more text")
;;    (p "another paragraph")
;; 
;; (p "testi\\ng again")
;; 
;; (div-id (p "hello world!")))
