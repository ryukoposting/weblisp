# Weblisp: A Sane, Lispy Web Framework

Weblisp is a minimal web framework. Its syntax is Lisp, and it
compiles to HTML and CSS. It is designed for
creating simple, elegant, and modern websites without ever having
to look at, let alone write, any HTML, CSS, or JavaScript.

Weblisp's primary features include:

 - Homogeneous syntax: Weblisp combines HTML, CSS, and CGI scripting
   into a single language. Instead of dealing with 3 different languages,
   plus external libraries, environment variables, and CGI parameter parsers,
   Weblisp handles everything in one simple syntax.
  
 - User-friendliness: Weblisp is designed to create lightweight
   websites that load quickly on client devices. Because no JavaScript
   is required for Weblisp to do its job, Weblisp sites provide a
   snappy user experience.

 - CSS Grid Support: Thanks to CSS Grid, website styles created
   in Weblisp are performant, predictable, and highly flexible.
   
 - Performance: Weblisp can be precompiled to an intermediate
   representation, allowing for superior runtime performance than
   could be achieved by having your webserver execute your weblisp
   scripts directly. Using these precompiled files, weblisp scripts
   run faster, reducing load on the server and reducing latency for
   users.

Weblisp was inspired in part by [werc](http://werc.cat-v.org/).

# Installing Weblisp
**(Recommended) Installing Weblisp from the Makefile**

Clone the repository...

`git clone https://gitlab.com/ryukoposting/weblisp`

Run the tests...

`cd weblisp/build`
`make test`

If all the tests pass...

`sudo make install`

**(Not Recommended) Using the GitLab CI Package Outputs**

Weblisp's CI pipeline automatically generates .elf executable files
and stores them for up to 24 hours after a commit to master. If you
check the recent CI outputs, you can get a precompiled executable 
there. However, these executables are not guaranteed to be particularly
stable, nor are they guaranteed to work properly on your machine.

# Objectives

Objectives broadly describe the purpose of weblisp. Check the
milestones for short-term, technical goals.

  - Full abstraction over HTML and CSS (that is, zero need for
    manually-written HTML or CSS, ever)

  - CGI scripting embedded seamlessly into the rest of the language.
  
  - Discourage, or outright eliminate, offloading of computation
    on user devices.

  - Allow not only for HTML webpages, but the creation of web API
    frontends using JSON.

# Example

This is a very basic weblisp script. Its behavior should be pretty
obvious just from reading it, as many of the expressions have names
similar to their HTML outputs:

```
;; todo.wl
(body
    (p "hello, " (env "USER"))

    (b "TODO:")
    (ul
        (li (st "argument parser"))
        (li "CSS expressions")
        (li "JSON expressions")
        (li "file import")
        (li "CGI string parser + CGI expression")
        (li (st "if statement"))
        (li "case statement?")
        (li "moar tests")
        (li "build cmd")
        (li "def expression/variable storage"))
    (img "https://pbs.twimg.com/media/CUNZWTTUcAAicyv.png" 
         (width 100)))
```

when precompiled using `weblisp pre todo.wl`, almost all of the HTML
expressions get collapsed into a single `raw` expression containing the
HTML. the `(env ...)` expression is not evaluated at precompile,
because the value of the environment variable may be different when the
script is run.

Now, instead of precompiling, try running the weblisp script. Then,
also try precompiling the script again, and piping the precompiled output
into weblisp again. *hint:* `weblisp --help`
