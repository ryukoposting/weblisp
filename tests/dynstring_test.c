/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "test.h"
#include "utils/dynstring.h"

TEST_BEGIN(dynstrings)
    String str;
    string_init(&str);
    
    test_asserteq(str.bytelen, 0);
    
    string_catbytes(&str, "hello world", 11);

    test_asserteq(string_bytelen(&str), 11);
    test_assert(strlen(string_ascharstr(&str)) == 11);
    test_assert(strcmp(string_ascharstr(&str), "hello world") == 0);
    
    string_catbytes(&str, ", this is a test", 16);
    test_asserteq(string_bytelen(&str), 27);
    test_assert(strlen(string_ascharstr(&str)) == 27);
    test_assert(strcmp(string_ascharstr(&str), "hello world, this is a test") == 0);
    
    char const longstring[] =
    "hello world, this is a test. this char array should "
    "cause a reallocation in the dynamic string because it is so long. "
    "this will allow us to verify that reallocations work properly. ";
    
    unsigned int longlen = strlen(longstring);
    
    string_catbytes(&str, &longstring[27], longlen - 27);
    test_asserteq(longlen, string_bytelen(&str));
    test_asserteq(longlen, strlen(string_ascharstr(&str)));
    test_assert(strcmp(string_ascharstr(&str), longstring) == 0);
    string_free(&str);
    
    string_init(&str);
    string_catbytes(&str, "%48%65%6c%6C123%6f%20world!", strlen("%48%65%6c%6C123%6f%20world!"));
    test_asserteq(string_unpackreqesc(&str), 0);
    test_assert(strcmp(string_ascharstr(&str), "Hell123o world!") == 0);
    
    test_asserteq(string_replacestr(&str, "OOOOO", "123"), 0);
    test_assert(strcmp(string_ascharstr(&str), "HellOOOOOo world!") == 0);
    
    test_asserteq(string_insertbytes(&str, "&&&", 17, 3), 0);
    test_assert(strcmp(string_ascharstr(&str), "HellOOOOOo world!&&&") == 0);
    
    test_asserteq(string_replacestr(&str, ">", "o"), 0);
    test_assert(strcmp(string_ascharstr(&str), "HellOOOOO> w>rld!&&&") == 0);
    
    test_asserteq(string_insertbytes(&str, "<", 0, 1), 0);
    test_assert(strcmp(string_ascharstr(&str), "<HellOOOOO> w>rld!&&&") == 0);
    
    test_asserteq(string_packhtmlesc(&str), 0);
    test_assert(strcmp(string_ascharstr(&str), "&lt;HellOOOOO&gt; w&gt;rld!&amp;&amp;&amp;") == 0);
    string_free(&str);
    
TEST_END
