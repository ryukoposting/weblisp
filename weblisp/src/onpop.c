/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "onpop/onpop_private.h"

BASIC_HTML_TAG_WRAP(head, "head")
BASIC_HTML_TAG_WRAP(body, "body")
BASIC_HTML_TAG_WRAP(st, "s")
BASIC_HTML_TAG_WRAP(address, "address")
BASIC_HTML_TAG_WRAP(article, "article")
BASIC_HTML_TAG_WRAP(aside, "aside")
BASIC_HTML_TAG_WRAP(code, "code")
BASIC_HTML_TAG_WRAP(cite, "cite")
BASIC_HTML_TAG_WRAP(details, "details")
BASIC_HTML_TAG_WRAP(figcaption, "figcaption")
BASIC_HTML_TAG_WRAP(figure, "figure")
BASIC_HTML_TAG_WRAP(footer, "footer")
BASIC_HTML_TAG_WRAP(header, "header")
BASIC_HTML_TAG_WRAP(main, "main")
BASIC_HTML_TAG_WRAP(mark, "mark")
BASIC_HTML_TAG_WRAP(nav, "nav")
BASIC_HTML_TAG_WRAP(section, "section")
BASIC_HTML_TAG_WRAP(style, "styles")
BASIC_HTML_TAG_WRAP(summary, "summary")
BASIC_HTML_TAG_WRAP(title, "title")
BASIC_HTML_TAG_WRAP(time, "time")
BASIC_HTML_TAG_WRAP(p, "p")
BASIC_HTML_TAG_WRAP(h1, "h1")
BASIC_HTML_TAG_WRAP(h2, "h2")
BASIC_HTML_TAG_WRAP(h3, "h3")
BASIC_HTML_TAG_WRAP(h4, "h4")
BASIC_HTML_TAG_WRAP(h5, "h5")
BASIC_HTML_TAG_WRAP(h6, "h6")
BASIC_HTML_TAG_WRAP(b, "strong")
BASIC_HTML_TAG_WRAP(i, "em")
BASIC_HTML_TAG_WRAP(ul, "ul")
BASIC_HTML_TAG_WRAP(ol, "ol")
BASIC_HTML_TAG_WRAP(u, "u")
BASIC_HTML_TAG_WRAP(li, "li")
// BASIC_HTML_TAG_WRAP(style, "style")

HTML_ATTRIBUTE_DIMENSION(width, "width", DATATYPE_WIDTH)
HTML_ATTRIBUTE_DIMENSION(height, "height", DATATYPE_HEIGHT)

/* TODO: make this less gross */
Data *ONPOP(img)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)
{
    UNUSED(flags); UNUSED(cstk); 
    
    unsigned int nchars = 0;
    int i = argc;
    Data const *it = argv;
    Data const *args[argc];
    
    if (argc < 1) {
        errmsg_print("illegal number of arguments for img", self);
        return NULL;
    }
    
    int has_width = 0, has_height = 0;
    
    while (i--) {
        assert(it != NULL);
        switch (it->type) {
        case DATATYPE_HEIGHT:
            if (has_height) {
                errmsg_print("multiple declarations of height", it->token);
                return NULL;
            }
            has_height = 1;
            break;
            
        case DATATYPE_WIDTH:
            if (has_width) {
                errmsg_print("multiple declarations of width", it->token);
                return NULL;
            }
            has_width = 1;
            break;
            
        case DATATYPE_STRING:
        case DATATYPE_RAW:
            break;
        
        default:
            errmsg_print("illegal argument type to img", it->token);
            return NULL;
        }
        
        assert(it->as_string != NULL);
        nchars += strlen(it->as_string);
        
        args[i] = it;
        it = it->next;
    }
    
    if ((args[0]->type != DATATYPE_RAW) && (args[0]->type != DATATYPE_STRING)) {
        errmsg_print("first argument of img must be string for image path", self);
        return NULL;
    }
    
    char *cat = malloc(sizeof(char) * (argc + nchars + 64));
    if (cat == NULL) {
        errmsg_print("failed to allocate memory for img", self);
        return NULL;
    }
    
    cat[0] = 0;
    sprintf(cat, "<img src=\\\"%s\\\" ", args[0]->as_string);
    
    ++i;
    while ((++i) < argc) {
        switch (args[i]->type) {
        case DATATYPE_WIDTH:
        case DATATYPE_STRING:
        case DATATYPE_RAW:
            strcat(cat, args[i]->as_string);
            break;
            
        default:
            break;
        }
    }
    
    strcat(cat, ">");
    
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_RAW;
    out->as_string = cat;

    return out;
}

Data *ONPOP(px)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)
{   
    UNUSED(flags); UNUSED(cstk); 
    
    if ((argc != 1) || (argv->type != DATATYPE_INTEGER)) {
        errmsg_print("px expects one integer argument", self);
        return NULL;
    }
    
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_DIMENSION_PX;
    out->as_int = argv->as_int;
    
    return out;
}

Data *ONPOP(bareword)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)
{
    Data *getfn = callstack_getdef(cstk, self->contents);
    
    if (getfn != NULL) {
        /* calling an already-defined function */
        if (getfn->type == DATATYPE_FUNCTION) {
            CallStack *cs = callstack_init();
            int a = argc;
            Data *value = argv;
            Data *name = getfn->as_function.argnames;
            
            while (a--) {
                /* argv (parameters to pass) */
                /* getfn->as_function.argv (parameter names) */
                definition_put(cs->defs, name->token->contents, value);
                name = name->next;
                value = value->next;
            }
            
            cs->next = cstk;
            Token *start = getfn->token;
            assert(parsers[start->id].flags & TPFLAGS_PUSH);
            Data *result = stacker_run(start, NULL, cs);
            return result;
        }
        return NULL;
        
    } else {
        /* this function is not defined. return a CALL type. */
        Data *out = data_init();
        out->token = self;
        out->type = DATATYPE_CALL;
        out->as_call.argv = argv;
        out->as_call.argc = argc;
        *flags = POPFLAG_DONT_FREE_ARGS;
        
        return out;
    }
}

Data *ONPOP(raw)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    UNUSED(flags); UNUSED(argv); UNUSED(cstk); 
    
    if (argc != 0) {
        errmsg_print("raw block must contain exactly one string literal", self);
        return NULL;
    }
    
    assert(argv == NULL);
    
    Data *out = data_init();
    out->token = self;
    out->type = DATATYPE_RAW;
    out->as_string = self->contents + 4;
    out->as_string += 1;
    out->as_string[strlen(out->as_string) - 1] = 0;
    
    return out;
}

Data *ONPOP(env)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    UNUSED(flags); UNUSED(cstk); 
    
    if (argc != 1 || (argv->type != DATATYPE_STRING)) {
        errmsg_print("env expects one string argument", self);
        return NULL;
    }

    char *env = getenv(argv->as_string);
    if (env == NULL) env = "";
    unsigned long long len = strlen(env) + 1;
    
    Data *out = data_init();
    out->token = self;
    out->type = DATATYPE_STRING;
    out->as_string = malloc(sizeof(char) * len);
    if (out->as_string == NULL) {
        errmsg_print("critical: out of memory", self);
        return NULL;
    }
    memcpy(out->as_string, env, len);
    
    return out;
}

Data *ONPOP(eq)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    UNUSED(flags); UNUSED(cstk);
    
    if (argc < 2) {
        errmsg_print("= expects at least 2 arguments", self);
        return NULL;
    }
    
    assert(argv != NULL);
    Data const *chkarg = argv->next;
    int carg = argc - 1;
    DataBool eq = DATABOOL_TRUE;
    
    unsigned long long hash = 0;
    if (argv->type == DATATYPE_STRING)
        hash = hash_fnv1a_64(argv->as_string, -1);
    
    while (--carg >= 0) {
        assert(chkarg != NULL);
        if ((chkarg->type != argv->type) &&
            !((chkarg->type == DATATYPE_INTEGER && argv->type == DATATYPE_FLOATING) ||
              (argv->type == DATATYPE_INTEGER && chkarg->type == DATATYPE_FLOATING))) {
            errmsg_print("= expects all arguments to be of the same type", self);
            return NULL;
        }
        if (chkarg->type == DATATYPE_FUNCTION) {
            errmsg_print("= cannot compare functions", self);
            return NULL;
        }
        
        if (!data_cmp(argv, chkarg, hash)) eq = DATABOOL_FALSE;

        chkarg = chkarg->next;
    }
    
    Data *out = data_init();
    out->token = self;
    out->type = DATATYPE_BOOL;
    out->as_bool = eq;
    
    return out;
}

Data *ONPOP(iff)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    UNUSED(cstk); 
    
    if (argc != 3) {
        errmsg_print("if expects exactly 3 arguments", self);
        return NULL;
    }

    Data *cond = argv->next->next;
    Data *tru = argv->next;
    Data *fals = argv;
    assert(argv->next->next->next == NULL);
    Data *out;

    cond->next = NULL;
    tru->next = NULL;
    fals->next = NULL;
    cond = eval(cond, cond->token->prev, cstk);
    
    if (cond->type != DATATYPE_BOOL) {
        errmsg_print("first argument of 'if' must be a boolean", self);
        return NULL;
    }
    
    if (cond->as_bool == DATABOOL_TRUE) {
        out = eval(tru, tru->token->prev, cstk);
        data_free(fals);
    } else {
        out = eval(fals, fals->token->prev, cstk);
        data_free(tru);
    }
    
    data_free(cond);
    
    *flags = POPFLAG_DONT_FREE_ARGS;
    return out;
}

Data *ONPOP(list)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    UNUSED(cstk); 
    
    if (argc < 1) {
        errmsg_print("list expects at least one argument", self);
        return NULL;
    }
    
    Data *out = data_init();
    out->type = DATATYPE_LIST;
    out->as_list.data = argv;
    out->token = self;
    
    *flags = POPFLAG_DONT_FREE_ARGS;
    
    return out;
}

Data *ONPOP(cons)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    UNUSED(cstk); 
    
    if (argc != 2) {
        errmsg_print("cons expects exactly two arguments", self);
        return NULL;
    }
    
    Data *out = data_init();
    out->type = DATATYPE_LIST;
    
    if (argv->type == DATATYPE_LIST) {
        Data *end = argv->as_list.data;
        for (; end && (end->next); end = end->next);
        end->next = argv->next;
        out->as_list.data = argv->as_list.data; /*LEAK?*/
    } else {
        out->as_list.data = argv;
    }
    out->token = self;
    
    *flags = POPFLAG_DONT_FREE_ARGS;
    
    return out;
}

Data *ONPOP(let)(Token *self, int argc, Data *argv, unsigned int *flags,
                 CallStack *cstk)
{
    if (argc < 2) {
        errmsg_print("let expects at least two arguments", self);
        if (argc == 0) hintmsg_print("did you use a reserved keyword as a variable name?", self->next);
        return NULL;
    }
    
    Data *fst = argv;
    Data *bfst = NULL;
    while (fst->next != NULL) {
        bfst = fst;
        fst = fst->next;
    }
    
    if ((fst->type != DATATYPE_CALL) && (fst->type != DATATYPE_SYMBOL)) {
        errmsg_print("invalid type for first argument of let", fst->token);
        return NULL;
    }
    
    *flags = POPFLAG_DONT_FREE_ARGS;
    
    Data *out = NULL;
    switch (fst->type) {
    case DATATYPE_CALL:
        if (!(parsers[bfst->token->prev->id].flags & TPFLAGS_PUSH)) {
            errmsg_print("invalid expression for function definition", bfst->token);
            return NULL;
        }
        out = data_init();
        out->type = DATATYPE_FUNCTION;
        out->as_function.argnames = fst->as_call.argv;
        out->as_function.name = fst->token->contents;
        out->token = bfst->token->prev;
        break;
    
    case DATATYPE_SYMBOL:
        out = bfst;
        break;
        
    default: assert(0);
    }
    
    assert(out != NULL);
    definition_put(cstk->defs, fst->token->contents, out);
    return out;
}
