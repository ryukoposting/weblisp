/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>
#include "parse.h"
#include "stack.h"
#include "onpop.h"
#include "onpush.h"
#include "definition.h"
#include "utils/errmsg.h"

void callstack_free(CallStack *stk)
{
    if (stk == NULL) return;
    assert(stk->next != stk);
    CallStack *next = stk->next;
    deftable_free(stk->defs);
    free(stk);
    if (next) callstack_free(next);
}


void data_free(Data *data)
{
    if (data == NULL) return;
    
    Data *next = data->next;
    
    if (data->defrc == 0) {
        if (data->type == DATATYPE_STRING) {
            free(data->as_string);
        } else if (data->type == DATATYPE_CALL) {
            data_free(data->as_call.argv);
        }
        
        assert(next != data);
        free(data);
    }
    
    data_free(next);
}

CallStack *callstack_init()
{
    CallStack *out = malloc(sizeof(CallStack));
    assert(out != NULL);
    
    out->token = NULL;
    out->defs = deftable_init();
    out->next = NULL;
    out->argc = 0;
    out->nopre = 0;
    out->lazy = 0;
    out->lazymember = 0;
    out->letexp = 0;
    out->letmember = 0;
    
    return out;
}

Data *callstack_getdef(CallStack const *stk, char const *str)
{
    while (stk != NULL) {
        Definition *o = definition_get(stk->defs, str);
        if (o != NULL) return o->data;
        stk = stk->next;
    }
    
    return NULL;
}
