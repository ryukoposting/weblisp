#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "definition.h"
#include "utils/hash.h"

DefTable *deftable_init()
{
    return calloc(1, sizeof(DefTable));
}

Definition *definition_create(char const *name, Data *data)
{
    Definition *out = malloc(sizeof(Definition));
    out->next = NULL;
    unsigned long long allocsz = (sizeof(char) * (strlen(name) + 1));
    out->name = malloc(allocsz);
    assert(out->name != NULL);
    memcpy(out->name, name, allocsz);
    out->data = data;
    data->defrc += 1;
    return out;
}

void definition_free(Definition *d)
{
    while (d != NULL) {
        Definition *f = d;
        d = d->next;
        f->data->defrc -= 1;
        free(f->name);
        free(f);
    }
}

void deftable_free(DefTable *dt)
{
    for (unsigned int i = 0; i < DEFINITION_HASHTABLE_SIZE; ++i) {
        definition_free(dt->table[i]);
    }
    
    free(dt->table);
}

int definition_put(DefTable *dt, char const *name, Data *data)
{   
    if ((dt == NULL) || (name == NULL) || (data == NULL)) return -1;
    
    unsigned int i = hash_fnv1a_32(name, -1) % DEFINITION_HASHTABLE_SIZE;
    
    Definition *newdef = definition_create(name, data);
    
    if (dt->table[i] == NULL) {
        dt->table[i] = newdef;
        return 0;
    }
    
    Definition *end = dt->table[i];
    while (end->next != NULL) {
        end = end->next;
    }
    assert(end->next == NULL);
    
    end->next = newdef;
    
    return 0;
}

Definition *definition_get(DefTable *dt, char const *name)
{
    unsigned int i = hash_fnv1a_32(name, -1) % DEFINITION_HASHTABLE_SIZE;
    
    Definition *out = dt->table[i];
    
    while (out != NULL) {
        assert(out->data->type != 0);
        if (strcmp(name, out->name) == 0) {
            return out;
        }
        assert(out->data->type != 0);
        out = out->next;
    }
    
    return out;
}

void definition_printcontents(DefTable const *dt)
{
    unsigned int i = 0;
    do {
        Definition *d = dt->table[i];
        while (d != NULL) {
            printf("%s=\n", d->name);
            d = d->next;
        }
    } while (++i < DEFINITION_HASHTABLE_SIZE);
}
