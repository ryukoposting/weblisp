/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "args/args.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "utils/dynstring.h"

#define RETURN_ARGHELP() do {\
    printf("%s\n", args_usagestring);\
    return -1; } while (0)

#define RETURN_ARGERR(msg) do {\
    printf("error: %s\nhint: try \"weblisp help\"\n", msg);\
    return -2; } while (0)

static char const args_usagestring[] =
"Usage: weblisp [MODE] [input_file] [-o output_file]\n"
"Options:\n"
"  MODE\n"
"      Set the mode to any one of the following.\n"
"          run   - execute the weblisp program.\n"
"          pre   - precompile the weblisp program.\n"
"      if MODE is not given, the default is run.\n\n"
"  input_file\n"
"      Path to a Weblisp file that will be processed.\n"
"      if input_file is not given, the default is the standard input.\n\n"
"  -o output_file\n"
"      Path to a file where the output will be written.\n"
"      if this directive is not given, the default is the standard output.\n\n"
"  help, --help\n"
"      Shows this screen.\n"
"\n"
"Weblisp v0.1\n"
"Copyright 2018 Evan Grove\n"
"Licensed under the Apache License, Version 2.0.\n"
"View the license here: http://www.apache.org/licenses/LICENSE-2.0";

char *args_inputstr(char const *path)
{
    char *outstr = NULL;
    
    if (path != NULL) {
        FILE *fp = fopen(path, "rb");
        if (fp == NULL) {
            fprintf(stderr, "file not found: %s\n", path);
            exit(0);
        }
        
        fseek(fp, 0, SEEK_END);
        int flen = ftell(fp);
        rewind(fp);
        outstr = malloc(sizeof(char) * (flen + 1));
        fread(outstr, flen, 1, fp);
        outstr[sizeof(char) * flen] = 0;
        fclose(fp);
        
    } else {
        String ds;
        int c;
        string_initsize(&ds, 2048);
        while ((c = fgetc(stdin)) != EOF) {
            char const ch = (c & 0xFF);
            string_catbytes(&ds, &ch, 1);
        }
        outstr = string_ascharstr(&ds);
    }
        
    return outstr;
}

FILE *args_outputfile(char const *path)
{
    if (path == NULL) return stdout;
    
    FILE *fp = fopen(path, "w+");
    if (fp == NULL) {
        fprintf(stderr, "failed to open output file: %s\n", path);
        exit(0);
    }
    
    return fp;
}

int args_parse(int argc, char **argv, ArgOptions *options)
{
    int skip_next = 0, n;
    
    if (argc > 5) RETURN_ARGERR("incorrect number of arguments");
    
    options->inpath = NULL;
    options->outpath = NULL;
    options->run = 0;
    options->pre = 0;
    
    if (argc == 1) {
        options->run = 1;
        return 0;
    }
    
    /* argv[1] is either pre, build, -o, infile */
    if (strcmp("-o", argv[1]) == 0) {
        options->run = 1;
        
        if (argc == 3) {
            options->outpath = argv[2];
            skip_next = 1;
        } else RETURN_ARGERR("no argument given for -o");
        
    } else if (strcmp("pre", argv[1]) == 0) {
        options->pre = 1;
    
    } else if (strcmp("run", argv[1]) == 0) {
        options->run = 1;
    
    } else if ((strcmp("--help", argv[1]) == 0) || (strcmp("help", argv[1]) == 0)) {
        RETURN_ARGHELP();
        
    } else {
        options->run = 1;
        options->inpath = argv[1];
    }
    
    /* argv 2-end are infile, -o, or outfile */
    for (n = 2 + skip_next; n < argc; n += (1 + skip_next)) {
        skip_next = 0;
        if (strcmp("-o", argv[n]) == 0) {
            if (((n + 1) < argc) && (options->outpath == NULL)) {
                options->outpath = argv[n + 1];
                skip_next = 1;
            } else RETURN_ARGERR("no filearg given for -o");
        
        } else if (options->inpath == NULL) {
            options->inpath = argv[n];
        
        } else RETURN_ARGERR("unrecognized argument pattern");
    }
    
    return 0;
}
