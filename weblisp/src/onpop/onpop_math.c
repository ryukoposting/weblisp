/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "onpop/onpop_private.h"

MATH_FLOATING_UNORDERED_OPERATOR(fplus, +, 0, 0);
MATH_FLOATING_UNORDERED_OPERATOR(fmult, *, 1, 0);

Data *ONPOP(fdiv)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)
{
    UNUSED(flags); UNUSED(cstk);
    
    double n = 1;
    if (argc < 1) {
        errmsg_print("div expects at least 1 argument", self);
        return NULL;
    
    } else {
        Data const *ds[argc];
        fill_arr_args(ds, argc, argv);
        int i = 0;
        
        if (argc > 1) {
            if (ds[0]->type == DATATYPE_INTEGER) {
                n = ds[0]->as_int;
            } else if (ds[0]->type == DATATYPE_FLOATING) {
                n = ds[0]->as_floating;
            } else {
                errmsg_print("non-numeric argument type", self);
                return NULL;
            }
            i = 1;
        }
        
        do {
            if (ds[i]->type == DATATYPE_INTEGER) {
                n /= ds[i]->as_int;
            } else if (ds[i]->type == DATATYPE_FLOATING) {
                n /= ds[i]->as_floating;
            } else {
                errmsg_print("non-numeric argument type", self);
                return NULL;
            }
        } while (++i < argc);
    }
    
    Data *out = data_init();
    out->token = self;
    out->type = DATATYPE_FLOATING;
    out->as_floating = n;
    return out;
}

Data *ONPOP(fminus)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)
{
    UNUSED(flags); UNUSED(cstk);
    
    double n = 0;
    if (argc < 1) {
        errmsg_print("minus expects at least 1 argument", self);
        return NULL;
    
    } else {
        Data const *ds[argc];
        fill_arr_args(ds, argc, argv);
        int i = 0;
        
        if (argc > 1) {
            if (ds[0]->type == DATATYPE_INTEGER) {
                n += ds[0]->as_int;
            } else if (ds[0]->type == DATATYPE_FLOATING) {
                n += ds[0]->as_floating;
            } else {
                errmsg_print("non-numeric argument type", self);
                return NULL;
            }
            i = 1;
        }
        
        do {
            if (ds[i]->type == DATATYPE_INTEGER) {
                n -= ds[i]->as_int;
            } else if (ds[i]->type == DATATYPE_FLOATING) {
                n -= ds[i]->as_floating;
            } else {
                errmsg_print("non-numeric argument type", self);
                return NULL;
            }
        } while (++i < argc);
    }
    
    Data *out = data_init();
    out->token = self;
    out->type = DATATYPE_FLOATING;
    out->as_floating = n;
    return out;
}
