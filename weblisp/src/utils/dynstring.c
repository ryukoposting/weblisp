/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "utils/dynstring.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <stdint.h>

#define DYNSTR_ALLOC 64
#define NORMALIZE_ALLOC(n) ((((n) / DYNSTR_ALLOC) + 1) * DYNSTR_ALLOC)

/* reallocate string to larger block of memory, if necessary */
static int string_expandto(String *str, uint32_t newlen)
{
    if (newlen < str->bytelen) return -1;
    
    if (str->allocsz <= newlen) { /* reallocate */
        uint32_t newsz = str->allocsz;
        
        do {
            newsz *= 2;
        } while ((newsz < newlen) && (newsz > str->allocsz));
        
        if (newsz <= str->allocsz) return -1;
        
        char *newmem = malloc(newsz);
        
        if (newmem == NULL) return -2;
        
        memcpy(newmem, str->ptr, str->bytelen);
        newmem[str->bytelen] = 0;
        free(str->ptr);
        str->ptr = newmem;
        str->allocsz = newsz;
    }
    
    return 0;
}

int string_init(String *str)
{
    if (str == NULL) return -1;
    
    str->ptr = malloc(sizeof(char) * DYNSTR_ALLOC);
    
    if (str->ptr == NULL) return -2;
    
    str->ptr[0] = 0;
    str->allocsz = DYNSTR_ALLOC;
    str->bytelen = 0;
    
    return 0;
}

int string_initsize(String *str, uint32_t bytes)
{
    if (str == NULL) return -1;
    
    uint32_t normalized = NORMALIZE_ALLOC(bytes);
    if (normalized < bytes) return -1;
    
    str->ptr = malloc(sizeof(char) * normalized);
    
    if (str->ptr == NULL) return -2;
    
    str->ptr[0] = 0;
    str->allocsz = normalized;
    str->bytelen = 0;
    
    return 0;
}

int string_catbytes(String *str, char const *chrs, uint32_t len)
{
    if ((str == NULL) || (str->ptr == NULL) || (chrs == NULL)) return -1;
    if ((len > 0xFFFFFFFF - str->bytelen) || (str->bytelen > 0xFFFFFFFF - len)) return -1;
    
    uint32_t newlen = len + str->bytelen;
    int res = string_expandto(str, newlen);
    if (res) return res;
    
    strncat(str->ptr, chrs, len);
    str->bytelen = newlen;
    str->ptr[newlen] = 0;

    return 0;
}

int string_insertbytes(String *str, char const *chrs, uint32_t index, uint32_t len)
{
    if ((str == NULL) || (str->ptr == NULL) || (chrs == NULL) || (index > str->bytelen)) return -1;
    if ((len > 0xFFFFFFFF - str->bytelen) || (str->bytelen > 0xFFFFFFFF - len)) return -1;
    
    uint32_t newlen = len + str->bytelen;
    int res = string_expandto(str, newlen);
    if (res) return res;
    
    for (uint32_t i = newlen; i >= (index + len); --i)
        str->ptr[i] = str->ptr[i - len];
    for (uint32_t i = 0; (i < len) && chrs[i]; ++i)
        str->ptr[i + index] = chrs[i];
    str->bytelen = newlen;
    str->ptr[newlen] = 0;

    return 0;
}

int string_replacestr(String *str, char const *newstr, char const *oldstr)
{
    if ((str == NULL) || (str->ptr == NULL) || (newstr == 0) || (oldstr == 0)) return -1;
    
    String out;
    string_initsize(&out, str->bytelen);
    char const *c = str->ptr;
    uint32_t const nlen = strlen(newstr), olen = strlen(oldstr);
    uint32_t skip = 0;
    while (*c) {
        int result = -1;
        if (skip) {
            result = 0;
            --skip;
        } else if (!strncmp(c, oldstr, olen)) {
            result = string_catbytes(&out, newstr, nlen);
            skip = olen - 1;
        } else result = string_catbytes(&out, c, 1);
        
        if (result) {
            string_free(&out);
            return -1;
        }
        ++c;
    }
    string_free(str);
    memcpy(str, &out, sizeof(String));
    return 0;
}

int string_packhtmlesc(String *s)
{
    int res;
    if ((res = string_replacestr(s, "&amp;", "&"))) return res;
    if ((res = string_replacestr(s, "&quot;", "\""))) return res;
    if ((res = string_replacestr(s, "&lt;", "<"))) return res;
    if ((res = string_replacestr(s, "&gt;", ">"))) return res;
    return 0;
}

int string_unpackhtmlesc(String *s)
{
    int res;
    if ((res = string_replacestr(s, "&", "&amp;"))) return res;
    if ((res = string_replacestr(s, "\"", "&quot;"))) return res;
    if ((res = string_replacestr(s, "<", "&lt;"))) return res;
    if ((res = string_replacestr(s, ">", "&gt;"))) return res;
    return 0;
}

int string_unpackreqesc(String *s)
{
    if (!s) return -1;
    
    String d;
    string_init(&d);
    
    char const *c = string_ascharstr(s);
    char escstr[3] = "\0\0\0";
    int escs = 0, err = 0;
    long int convertchr = 0;
    while (c && (*c) && (!err)) {
        if ((*c == '%') && (!escs)) {
            escs = 2;
        
        } else if ((*c == '+') && (!escs)) {
            string_catbytes(&d, " ", 1);
        
        } else if (escs && isxdigit(*c)) {
            escstr[2 - escs] = *c;
            --escs;
            if (!escs) {
                convertchr = strtol(escstr, NULL, 16);
                assert(sprintf(escstr, "%c", (char)(convertchr & 0xFF)) == 1);
                string_catbytes(&d, escstr, 1);
            }

        } else if ((!escs) && (*c != ' ')) {
            assert(sprintf(escstr, "%c", (char)(*c & 0xFF)) == 1);
            string_catbytes(&d, escstr, 1);
            
        } else err = 1;
        
        ++c;
    }
    
    if ((!c) || err) {
        string_free(&d);
        return -1;
    }
    
    string_free(s);
    memcpy(s, &d, sizeof(String));
    return 0;
}
