/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "utils/hash.h"
#include <assert.h>

unsigned int hash_fnv1a_32(char const *instr, int len)
{
    assert(len >= -1);
    if (len == -1) len = 0xFFFFFFFF;
    
    unsigned int hash = 0x811c9dc5, i = 0;
    
    while ((len--) && instr[i]) {
        hash ^= instr[i];
        hash *= 16777619;
        ++i;
    }
    
    return hash;
}

unsigned long long hash_fnv1a_64(char const *instr, int len)
{
    assert(len >= -1);
    if (len == -1) len = 0xFFFFFFFF;
    
    unsigned int i = 0;
    unsigned long long hash = 0xcbf29ce484222325;
    
    while ((len--) && instr[i]) {
        hash ^= instr[i];
        hash *= 1099511628211;
        ++i;
    }
    
    return hash;
}
