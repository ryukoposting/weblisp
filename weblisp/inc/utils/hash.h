/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _HASH_H_
#define _HASH_H_

/* all functions declared in this file work like this:
- if len == -1, hash instr until the null terminator is found
- if len < -1, throw an assertion error
- otherwise, hash up to len bytes 
- all functions, regardless of len, will hash up to
  2,148,483,647 bytes (~2GiB) before stopping. */

#ifdef __cplusplus
extern "C" {
#endif

unsigned int hash_fnv1a_32(char const *instr, int len);

unsigned long long hash_fnv1a_64(char const *instr, int len);

#ifdef __cplusplus
}
#endif

#endif
