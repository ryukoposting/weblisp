/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _DYNSTRING_H_
#define _DYNSTRING_H_

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct String {
    char *ptr;
    uint32_t allocsz;
    uint32_t bytelen;
} String;

int string_init(String *str);

int string_initsize(String *str, uint32_t bytes);

int string_catbytes(String *str, char const *chrs, uint32_t len);

int string_insertbytes(String *str, char const *chrs, uint32_t index, uint32_t len);

int string_packhtmlesc(String *s);

int string_unpackhtmlesc(String *s);

/* de-escape an HTTP request string. */
int string_unpackreqesc(String *s);

int string_replacestr(String *str, char const *newstr, char const *oldstr);

static inline uint32_t string_len(String *str)
{
    if (str == NULL) return -1;
    
    char const *cp = str->ptr;
    if (cp == NULL) return -1;
    
    unsigned int lchc = 0;
    uint32_t out = 0;
    
    while (*cp) {
        unsigned char const c = *cp & 0xFF;
        
        if (lchc == 0) {
            if (c < 0x80) {
                ++out; /* TODO: the 3 elifs can be one loop? */
            } else if ((c & 0xF8) == 0xF0) {
                ++out;
                lchc = 3;
            } else if ((c & 0xF0) == 0xE0) {
                ++out;
                lchc = 2;
            } else if ((c & 0xE0) == 0xC0) {
                ++out;
                lchc = 1;
            } else {
                return -1;
            }
            ++cp;
        } else if ((lchc <= 3) && ((c & 0xC0) == 0x80)) {
            --lchc;
        } else {
            return -1;
        }
    }
    
    return out;
}

static inline uint32_t string_bytelen(String *str)
{
    return str->bytelen;
}

static inline char *string_ascharstr(String *str)
{
    if ((str == NULL) || (str->ptr == NULL)) return NULL;
    str->ptr[str->bytelen] = 0;
    return str->ptr;
}

static inline int string_countchar(String *str, char c)
{
    char const *cp = str->ptr;
    int out = 0;
    while (*cp) {
        if (out == INT_MAX) return -1;
        if (*cp == c) ++out;
        ++cp;
    }
    return out;
}

static inline void string_free(String *str)
{
    free(str->ptr);
    str->ptr = NULL;
}

#ifdef __cplusplus
}
#endif

#endif
