/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _ERRMSG_H_
#define _ERRMSG_H_

#include "parse.h"

/* TODO: have WL_NDEBUG enabled during release builds. normal NDEBUG makes regexes segfault */
#ifndef WL_NDEBUG
#define debugmsg_print(fmt, ...) do {\
        fprintf(stdout, "DEBUG (" __FILE__ ":" STRINGIFY(__LINE__) ") " fmt, __VA_ARGS__);\
    } while (0)
#else
#define debugmsg_print(fmt, ...) do {} while(0)
#endif

#ifdef __cplusplus
extern "C" {
#endif


void errmsg_print(char const *msg, Token const *token);

void hintmsg_print(char const *msg, Token const *token);


#ifdef __cplusplus
}
#endif

#endif
