/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _MISC_H_
#define _MISC_H_

#define UNUSED(X) ((void)X)


#define STRINGIFY___(...) #__VA_ARGS__
#define STRINGIFY__(...) STRINGIFY___(__VA_ARGS__)
#define STRINGIFY_(...) STRINGIFY__(__VA_ARGS__)
#define STRINGIFY(...) STRINGIFY_(__VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif
