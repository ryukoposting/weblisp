/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _ONPOP_H_
#define _ONPOP_H_

#include "parse.h"
#include "stack.h"
#include <stdio.h>
#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>

#define ONPOP(name_) name_ ## _onpop

#define ONPOP_DECL(name_) Data *ONPOP(name_)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)

#ifdef __cplusplus
extern "C" {
#endif

typedef enum PopFlags {
    POPFLAG_DONT_FREE_ARGS = 0x8000000,
} PopFlags;

/* HTML TAGS */
ONPOP_DECL(head);
ONPOP_DECL(body);
ONPOP_DECL(p);
ONPOP_DECL(h1);
ONPOP_DECL(h2);
ONPOP_DECL(h3);
ONPOP_DECL(h4);
ONPOP_DECL(h5);
ONPOP_DECL(h6);
ONPOP_DECL(address);
ONPOP_DECL(article);
ONPOP_DECL(aside);
ONPOP_DECL(details);
ONPOP_DECL(figcaption);
ONPOP_DECL(figure);
ONPOP_DECL(footer);
ONPOP_DECL(header);
ONPOP_DECL(main);
ONPOP_DECL(mark);
ONPOP_DECL(nav);
ONPOP_DECL(section);
ONPOP_DECL(summary);
ONPOP_DECL(title);
ONPOP_DECL(time);
ONPOP_DECL(em);
ONPOP_DECL(strong);
ONPOP_DECL(code);
ONPOP_DECL(st);
ONPOP_DECL(raw);
ONPOP_DECL(b);
ONPOP_DECL(i);
ONPOP_DECL(ul);
ONPOP_DECL(ol);
ONPOP_DECL(li);
ONPOP_DECL(env);
ONPOP_DECL(cite);
ONPOP_DECL(img);

/* HTML ATTRIBUTES */
ONPOP_DECL(width);
ONPOP_DECL(height);

/* STYLES */
ONPOP_DECL(px);
ONPOP_DECL(style);
ONPOP_DECL(rows);
ONPOP_DECL(columns);
ONPOP_DECL(region);

/* ARITHMETIC/LOGIC OPERATIONS */
ONPOP_DECL(eq);
ONPOP_DECL(fplus);
ONPOP_DECL(fminus);
ONPOP_DECL(fmult);
ONPOP_DECL(fdiv);

/* CONTROL FLOW */
ONPOP_DECL(iff);

/* MISC */
ONPOP_DECL(bareword);
ONPOP_DECL(list);
ONPOP_DECL(cons);
ONPOP_DECL(let);
ONPOP_DECL(assert);

#ifdef __cplusplus
}
#endif

#endif
