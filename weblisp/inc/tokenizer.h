/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _PARSE_PRIVATE_H_
#define _PARSE_PRIVATE_H_

#ifdef __cplusplus
extern "C" {
#endif

/* parsers are run in the order they are listed here. */
typedef enum TokenId {
    TOKENID_COMMENT,
    TOKENID_SHEBANG,
    TOKENID_STRING,
    TOKENID_OPENPAREN,
    TOKENID_CLOSEPAREN,

    TOKENID_FLOATING,
    TOKENID_INT_BASE16,
    TOKENID_INT_BASE2,
    TOKENID_INT_BASE10,
    TOKENID_BOOL,
    
    TOKENID_RAW, /* should be high-priority to improve run speed */
    TOKENID_ENV,
    
    TOKENID_LIST,
    TOKENID_CONS,
    TOKENID_LET,
    TOKENID_IF,
    TOKENID_EQ,
    TOKENID_PLUS,
    TOKENID_MINUS,
    TOKENID_ASTERISK,
    TOKENID_FWDSLASH,
  
    TOKENID_STYLE,
    TOKENID_H1,
    TOKENID_H2,
    TOKENID_H3,
    TOKENID_H4,
    TOKENID_H5,
    TOKENID_H6,
    TOKENID_UL,
    TOKENID_OL,
    TOKENID_B,
    TOKENID_I,
    TOKENID_CITE,
    TOKENID_LI,
    TOKENID_ST,
    TOKENID_PARAGRAPH,
    TOKENID_PX,
    TOKENID_IMG,
    TOKENID_WIDTH,
    TOKENID_HEIGHT,
    TOKENID_HEAD,
    TOKENID_BODY,
    TOKENID_MAIN,
    TOKENID_SECTION,
    TOKENID_NAV,
    TOKENID_HEADER,
    TOKENID_FOOTER,
    TOKENID_ARTICLE,
    TOKENID_ADDRESS,
    TOKENID_ASIDE,
    TOKENID_CODE,
    TOKENID_SUMMARY,
    TOKENID_TITLE,
    TOKENID_MARK,
    TOKENID_TIME,
    TOKENID_DETAILS,
    TOKENID_FIGURE,
    TOKENID_FIGCAPTION,
    TOKENID_ASSERT,
    
    TOKENID_BAREWORD,
    TOKENID_WHITESPACE,
    
    PARSERS_LENGTH
} TokenId;

typedef enum TPFlags {
    TPFLAGS_DISCARD = 0x1,
    TPFLAGS_ATOM = 0x2,
    TPFLAGS_PUSH = 0x4,
    TPFLAGS_POP = 0x8,
    TPFLAGS_NOPRE = 0x10, /* tokens that should only be evaluated on run */
    TPFLAGS_FUNC = 0x20,
    TPFLAGS_LET = 0x40, /* lazy, but also put the expression in definition hashtable */
    TPFLAGS_LAZY = 0x80 /* (NYI) don't evaluate children until required for a non-lazy eval */
} TPFlags;

typedef struct Token {
    TokenId id;
    char const *keystr;
    char *contents;
    unsigned int lineno;
    struct Token *next;
    struct Token *prev;
} Token;

#ifdef __cplusplus
}
#endif

#endif
