/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _ONPOP_PRIVATE_H_
#define _ONPOP_PRIVATE_H_

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include "onpop.h"
#include "utils/errmsg.h"
#include "utils/stripstr.h"
#include "utils/hash.h"
#include "utils/dynstring.h"

#ifdef __cplusplus
extern "C" {
#endif
 /* takes a Data* of type DATATYPE_LIST, using HTML escape sequences (but not on raws) */
char *flatten_list_html(Data const *lstdat);

/* takes an ordered array of Data*s, flattens them into a string using HTML escape sequences (but not on raws) */
char *flatten_dataarr_html(Data const **arrdat, unsigned int len);

static inline Data *eval(Data *in, Token *start, CallStack *cstk)
{
    if (in->type == DATATYPE_CALL) {
        assert(parsers[start->id].flags & TPFLAGS_PUSH);
        return eval_dispatcher(start, cstk);
    }
    else return in;
}
/* argv is passed onto ONPOP(...)'s in reverse order. This function fills an array
in the normal order (i.e. first argument to an expr is in element 0) */
static inline void fill_arr_args(Data const *argarr[static 1], int argc, Data const *argv)
{
    assert(argc > 0);
    while (argc--) {
        assert(argv != NULL);
        argarr[argc] = argv;
        argv = argv->next;
    }
}


#define BASIC_HTML_TAG_WRAP(_tag,_html)\
    Data *ONPOP(_tag)(Token *self, int argc, Data *argv, unsigned int *flags,\
                 CallStack *cstk)\
    {\
        UNUSED(flags); UNUSED(cstk); \
        static const char tag1[] = "<" _html ">\n";\
        static const char tag2[] = "\n</" _html ">";\
        \
        int i = argc;\
        Data const *it = argv;\
        Data const *args[argc];\
        \
        while (i--) {\
            assert(it != NULL);\
            if ((it->type != DATATYPE_STRING) && (it->type != DATATYPE_RAW) && (it->type != DATATYPE_LIST)) {\
                errmsg_print("illegal argument type to " STRINGIFY(_tag), it->token);\
                return NULL;\
            }\
            args[i] = it;\
            it = it->next;\
        }\
        \
        String cat;\
        string_init(&cat);\
        \
        string_catbytes(&cat, tag1, sizeof(tag1) * sizeof(char));\
        char *content = flatten_dataarr_html(args, argc);\
        if (!content) {\
            hintmsg_print("error occurred in this expression", self);\
            return NULL;\
        }\
        string_catbytes(&cat, content, strlen(content));\
        string_catbytes(&cat, tag2, sizeof(tag2) * sizeof(char));\
        \
        Data *out = data_init();\
        \
        out->token = self;\
        out->type = DATATYPE_RAW;\
        out->as_string = string_ascharstr(&cat);\
        \
        return out;\
    }

#define HTML_ATTRIBUTE_DIMENSION(_name,_html,_type)\
    Data *ONPOP(_name)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)\
    {\
        UNUSED(flags); UNUSED(cstk); \
        if (argc != 1) {\
            errmsg_print(STRINGIFY(_name) " expects exactly 1 argument", self);\
            return NULL;\
        }\
        Data *out;\
        \
        switch (argv->type) {\
        case DATATYPE_INTEGER:\
        case DATATYPE_DIMENSION_PX:\
            out = data_init();\
            out->token = self;\
            out->type = _type;\
            out->as_string = malloc(sizeof(char) * ((argv->as_int / 10) + 8) + sizeof(_html));\
            sprintf(out->as_string, _html "=\\\"%d\\\"", argv->as_int);\
            break;\
        \
        case DATATYPE_AUTO:\
            out = data_init();\
            out->token = self;\
            out->type = _type;\
            out->as_string = malloc(sizeof(char) * 1); /* all strings must be free-able currently */\
            out->as_string[0] = 0;\
            break;\
            \
        default:\
            errmsg_print("first argument of " STRINGIFY(_name) " must be a dimension or integer value", self);\
            return NULL;\
        }\
\
        return out;\
    }\

#define MATH_FLOATING_UNORDERED_OPERATOR(_name,_oper,_initval,_minargs)\
    Data *ONPOP(_name)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)\
    {\
        UNUSED(flags); UNUSED(cstk);\
        if (argc < _minargs) {\
            errmsg_print(STRINGIFY(_name) " expects at least " STRINGIFY(_minargs) " arguments", self);\
            return NULL;\
        }\
        double n = _initval;\
        \
        Data *d = argv;\
        int i = argc;\
        while (i--) {\
            assert(d != NULL);\
            switch (d->type) {\
            case DATATYPE_INTEGER:\
                n = n _oper d->as_int;\
                break;\
            \
            case DATATYPE_FLOATING:\
                n = n _oper d->as_floating;\
                break;\
            \
            default:\
                errmsg_print("non-numerical argument type", self);\
                return NULL;\
            }\
            d = d->next;\
        }\
        Data *out = data_init();\
        out->token = self;\
        out->type = DATATYPE_FLOATING;\
        out->as_floating = n;\
        return out;\
    }

#ifdef __cplusplus
}
#endif

#endif
