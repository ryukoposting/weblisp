/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifdef LINUX /* required to eliminate superfluous warning messages */
#define _XOPEN_SOURCE 500
#endif

#include "parse.h"
#include "utils/errmsg.h"
#include "utils/lazyregex.h"
#include "utils/dynstring.h"
#include "args/args.h"
#include "cgi/cgi.h"
#include <assert.h>
#include <regex.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ftw.h>
#include <sys/types.h>

extern void printpre(FILE *fp, Data *data);
extern void printrun(FILE *fp, Data *data);

int main(int argc, char **argv)
{
    ArgOptions opts;
    if (args_parse(argc, argv, &opts)) exit(0);
    
    char *str = args_inputstr(opts.inpath);
    FILE *outfp = args_outputfile(opts.outpath);
    
    assert(str != NULL);
    assert(outfp != NULL);
    
    Token *const head = tokenizer_run(str);

    if (head == NULL) {
        errmsg_print("Tokenization failed.", NULL);
        exit(1);
    }
    
    Data *result = stacker_run(head, &opts, callstack_init());
    int exitcode = 1;
    if (result != NULL) {
        if (opts.run)
            printrun(outfp, result);
        else if (opts.pre)
            printpre(outfp, result);
        
        if (opts.outpath != NULL) {
            fclose(outfp);
        }
        exitcode = 0;
    }
        
    data_free(result);
    free(str);
    tokenizer_free(head);
    exit(exitcode);
}

