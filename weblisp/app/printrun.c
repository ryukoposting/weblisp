/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>
#include "parse.h"
#include "utils/hash.h"
#include "utils/stripstr.h"
#include "utils/errmsg.h"

void printrun_iter(FILE *fp, Data *dat, unsigned char delimiter)
{
    if (dat == NULL) return;
    if (dat->next != NULL) printrun_iter(fp, dat->next, delimiter);
    
    switch (dat->type) {
    case DATATYPE_STRING:
    case DATATYPE_RAW:
        stripstr_escapes(dat->as_string, strlen(dat->as_string));
        fprintf(fp, "%s", dat->as_string);
        fputc(delimiter, fp);
        break;
    
    case DATATYPE_BOOL:
        if (dat->as_bool == DATABOOL_TRUE) fprintf(fp, "true ");
        else fprintf(fp, "false ");
        fputc(delimiter, fp);
        break;
    
    case DATATYPE_INTEGER:
        fprintf(fp, "%d ", dat->as_int);
        fputc(delimiter, fp);
        break;
    
    case DATATYPE_FLOATING:
        fprintf(fp, "%f ", dat->as_floating);
        fputc(delimiter, fp);
        break;
    
    case DATATYPE_LIST:
        printrun_iter(fp, dat->as_list.data, ' ');
        fprintf(fp, "%c", delimiter);
        break;
        
    case DATATYPE_FUNCTION:
    case DATATYPE_CALL:
    case DATATYPE_NONE:
        break;
    
    default:
        debugmsg_print("ERROR! on type %d\n", dat->type);
        assert(0);
    }
}

void printrun(FILE *fp, Data *data)
{
    printrun_iter(fp, data, '\n');
}
