/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>
#include "parse.h"
#include "utils/hash.h"

void printtoks(Token *tok)
{
    unsigned int i = 0;
    if (tok == NULL) return;
    do {
        if (parsers[tok->id].flags & TPFLAGS_PUSH) ++i;
        else if (parsers[tok->id].flags & TPFLAGS_POP) --i;
        printf("%s ", tok->contents);
        tok = tok->next;
    } while (i);
}

void printpre_iter(FILE *fp, Data *dat, unsigned int indent)
{
    if (dat == NULL) return;
    if (dat->next != NULL) printpre_iter(fp, dat->next, indent);
    
    fputc('\n', fp);
    for (unsigned int i = 0; i < indent; ++i) fputc(' ', fp);
    
    switch (dat->type) {
    /* exprs */
    case DATATYPE_FUNCTION:
        fprintf(fp, "(let (%s ", dat->as_function.name);
        printpre_iter(fp, dat->as_function.argnames, indent + 2);
        fprintf(fp, ")");
        printtoks(dat->token);
        fprintf(fp, ")");
        break;

    case DATATYPE_CALL:
        fprintf(fp, "(%s ", dat->token->contents);
        printpre_iter(fp, dat->as_call.argv, indent + 2);
        fprintf(fp, ")");
        break;
    
    case DATATYPE_WIDTH:
    case DATATYPE_HEIGHT:
    case DATATYPE_RAW:
        fprintf(fp, "(raw \"%s\")", dat->as_string);
        break;
    
    case DATATYPE_LIST:
        fprintf(fp, "(list ");
        printpre_iter(fp, dat->as_list.data, indent + 2);
        fprintf(fp, ")");
        break;
        
    case DATATYPE_DIMENSION_PX:
        fprintf(fp, "(%s %d)", dat->token->keystr, dat->as_int);
        break;
    
    /* atoms */
    case DATATYPE_BOOL:
        if (dat->as_bool == DATABOOL_TRUE) fprintf(fp, "true ");
        else fprintf(fp, "false ");
        break;
    
    case DATATYPE_STRING:
        fprintf(fp, "\"");
        fprintf(fp, "%s", dat->as_string);
        fprintf(fp, "\"");
        break;
    
    case DATATYPE_INTEGER:
        fprintf(fp, "%d ", dat->as_int);
        break;
    
    case DATATYPE_FLOATING:
        fprintf(fp, "%f ", dat->as_floating);
        break;
    
    case DATATYPE_SYMBOL:
        fprintf(fp, "%s ", dat->as_string);
        break;
    
    case DATATYPE_AUTO:
        fprintf(fp, "auto ");
        break;
    
    case DATATYPE_NONE: break;
    default:
        assert(0);
    }
}



void printpre(FILE *fp, Data *data)
{
    printpre_iter(fp, data, 0);
    fputc('\n', fp);
}

